﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Dot : MonoBehaviour
{
    [SerializeField] MeshRenderer meshRenderer;
    [SerializeField] MeshRenderer fogMeshRenderer;
    [SerializeField] Animator animator;
    [SerializeField] ParticleSystem birthEffect;
    [SerializeField] ParticleSystem deathEffect;
    [SerializeField] ParticleSystem deathEffect2;
    [SerializeField] ParticleSystem starSmokeEffect;
    [SerializeField] float speed;
    Color color;
    Vector2Int position;

    public Color Color { get { return color; } }
    public Vector2Int Position { get { return position; }}

    // Start is called before the first frame update
    void Start()
    {
        
    }

    public void Activate(List<Color> availableColors, Vector2Int position)
    {
        Color newColor = availableColors[Random.Range(0, availableColors.Count)];
        SetColor(newColor);
        SetPosition(position);
        this.gameObject.SetActive(true);
        animator.Play("Birth");

        birthEffect.gameObject.SetActive(true);
        birthEffect.Play();

        deathEffect.gameObject.SetActive(false);
        deathEffect.Stop();

        deathEffect2.gameObject.SetActive(false);
        deathEffect2.Stop();

        starSmokeEffect.Play();
        UpdateSmokeEffect();
    }

    public void Deactivate()
    {
        StartCoroutine(DeathCoroutine());

        animator.Play("Death");

        deathEffect.gameObject.SetActive(true);
        deathEffect.Play();

        deathEffect2.gameObject.SetActive(true);
        deathEffect2.Play();

        birthEffect.Stop();
        birthEffect.gameObject.SetActive(false);

        starSmokeEffect.Stop();
    }

    IEnumerator DeathCoroutine()
    {
        yield return new WaitForSeconds(3);
        this.gameObject.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    { 
        UpdateMovement();
        UpdateSmokeEffect();
    }

    void UpdateMovement()
    {
        float distance = Vector2.Distance(this.transform.localPosition, position);
        if (distance > speed * Time.deltaTime)
        {
            Vector2 direction = ((Vector2)position - (Vector2)this.transform.localPosition).normalized;
            this.transform.localPosition += (Vector3)direction * speed * Time.deltaTime;
        }
    }

    void UpdateSmokeEffect()
    {
        starSmokeEffect.transform.localScale = this.transform.localScale * 2;
    }

    public void SetPosition(Vector2Int position)
    {
        this.position = position;
        this.transform.localPosition = (Vector2)position;
    }

    public void MoveToPosition(Vector2Int position)
    {
        this.position = position;
    }

    public void SetColor(Color color)
    {
        this.color = color;
        meshRenderer.material.SetColor("_Color", color);
        meshRenderer.material.SetFloat("_StartRandom", Random.Range(0f,60f));
        fogMeshRenderer.material.SetColor("_Color", color);
        fogMeshRenderer.material.SetFloat("_StartRandom", Random.Range(0f, 60f));

        var birthEffectMain = birthEffect.main;
        Color birthEffectColor = color + new Color(0.5f, 0.5f, 0.5f, 1);
        birthEffectMain.startColor = birthEffectColor;

        var deathEffectMain = deathEffect.main;
        deathEffectMain.startColor = birthEffectColor;

        var deathEffect2Main = deathEffect2.main;
        deathEffect2Main.startColor = birthEffectColor;

        var smokeEffectMain = starSmokeEffect.main;
        Color smokeEffectColor = color + new Color(0.25f, 0.25f, 0.25f, 0);
        smokeEffectColor.a = smokeEffectMain.startColor.color.a;
        smokeEffectMain.startColor = smokeEffectColor;
    }

    public void Connect()
    {
        fogMeshRenderer.gameObject.SetActive(true);
    }

    public void Disconnect()
    {
        fogMeshRenderer.gameObject.SetActive(false);
    }

    
}
