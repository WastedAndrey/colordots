﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PrefabManager : MonoBehaviour
{
    public GameObject prefabDot;
    public GameObject prefabLightning;

    private static PrefabManager _instance;

    public static PrefabManager Instance
    {
        get
        {
            if (_instance == null)
            {
                _instance = GameObject.FindObjectOfType<PrefabManager>();
            }

            return _instance;
        }
    }

    private void Awake()
    {
        if (_instance != null) Destroy(this.gameObject);
        if (_instance == null) _instance = this;

        DontDestroyOnLoad(this.gameObject);
    }

}
