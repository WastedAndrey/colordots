﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LightningEffect : MonoBehaviour
{
    [SerializeField] List<LineRenderer> lineRenderers = new List<LineRenderer>();
    [SerializeField] float speed = 50;
    [SerializeField] Vector2 randomOffset = new Vector2(0.05f, 0.03f);
    [SerializeField] Vector3 start = Vector3.zero;
    [SerializeField] Vector3 finish = Vector3.zero;

    // Start is called before the first frame update
    void Awake()
    {
        for (int i = 0; i < lineRenderers.Count; i++)
        {
            lineRenderers[i].positionCount = 10;
        }
    }

    // Update is called once per frame
    void Update()
    {
        for (int i = 0; i < lineRenderers.Count; i++)
        {
            UpdateLineRenderer(lineRenderers[i]);
        }
    }

    public void Activate()
    {
        this.gameObject.SetActive(true);

        for (int i = 0; i < lineRenderers.Count; i++)
        {
            for (int j = 0; j < lineRenderers[i].positionCount; j++)
            {
                lineRenderers[i].SetPosition(j, Vector3.zero);
            }
        }
    }

    public void Deactivate()
    {
        this.gameObject.SetActive(false);
    }

    public void SetPositions(Vector3 start, Vector3 finish)
    {
        this.start = start;
        this.finish = finish;
    }

    void UpdateLineRenderer(LineRenderer lineRenderer)
    {
        float distance = Vector3.Distance(finish, start);
        if (distance == 0) return;

        Vector3 direction = (finish - start).normalized;
       

        Vector3[] positions = new Vector3[lineRenderer.positionCount];
        lineRenderer.GetPositions(positions);

        positions[0] = start;
        for (int i = 1; i < positions.Length - 1; i++)
        {
            Vector3 newPos = positions[i];
            Vector3 defaultPos = start + direction * distance *  i / positions.Length;
            Vector3 offset = Vector3.zero;

            newPos.x += Random.Range(-randomOffset.x, randomOffset.x) * Time.deltaTime * speed;
            newPos.y += Random.Range(-randomOffset.y, randomOffset.y) * Time.deltaTime * speed * 3;

            newPos.x = Mathf.Clamp(newPos.x, defaultPos.x - randomOffset.x, defaultPos.x + randomOffset.x);
            newPos.y = Mathf.Clamp(newPos.y, defaultPos.y - randomOffset.y, defaultPos.y + randomOffset.y);
            positions[i] = newPos;
        }
        positions[positions.Length - 1] = finish;

        lineRenderer.SetPositions(positions);
    }
}
