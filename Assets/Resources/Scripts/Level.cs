﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Level : MonoBehaviour
{
    [SerializeField] ObjectPool dotsPool;
    [SerializeField] DotConnector dotConnector;
    [SerializeField] Transform dotsParentTransform;

    [SerializeField]List<Color> availableColors = new List<Color>();
    Dot[,] dots = new Dot[6, 6];


    int score = 0;
    float recentScore = 0;
    float recentScoreTimer = 0;
    float recentScoreTimerDefault = 1.5f;

    public int Score { get { return score; } }
    public float RecentScore { get { return recentScore; } }


    bool inputAvailble = false;

    // Start is called before the first frame update
    void Awake()
    {
        dotsPool.Init(PrefabManager.Instance.prefabDot, 36);

        dotConnector.connectionFinished += FinishConnection;
    }

    public void Init()
    {
        for (int i = 0; i < dots.GetLength(0); i++)
        {
            for (int j = 0; j < dots.GetLength(1); j++)
            {
                CreateDot(new Vector2Int(i, j));
            }
        }
    }


    public void Init(SaveData saveData)
    {
        score = saveData.score;
        int colorListIndex = 0;
        for (int i = 0; i < dots.GetLength(0); i++)
        {
            for (int j = 0; j < dots.GetLength(1); j++)
            {
                Dot newDot = CreateDot(new Vector2Int(i, j));
                newDot.SetColor(saveData.colors[colorListIndex]);
                colorListIndex++;
            }
        }
    }

    public void StartGame()
    {
        SetInputAvailable(true);
    }

    public void Restart()
    {
        for (int i = 0; i < dots.GetLength(0); i++)
        {
            for (int j = 0; j < dots.GetLength(1); j++)
            {
                if (dots[i, j] != null) dots[i, j].Activate(availableColors, new Vector2Int(i, j));
                else CreateDot(new Vector2Int(i, j));
            }
        }

        score = 0;
        recentScore = 0;
    }

    public SaveData GetSaveData()
    {
        List<Color> colors = new List<Color>();
        for (int i = 0; i < dots.GetLength(0); i++)
        {
            for (int j = 0; j < dots.GetLength(1); j++)
            {
                if (dots[i, j] != null) colors.Add(dots[i, j].Color);
                else colors.Add(availableColors[Random.Range(0, availableColors.Count)]);
            }
        }

        SaveData newSaveData = new SaveData(score, colors);
        return newSaveData;
    }

    // Update is called once per frame
    void Update()
    {
        UpdateRecentScore();
    }

    void UpdateRecentScore()
    {
        if (recentScore > 0)
        {
            recentScoreTimer -= Time.deltaTime;
            if (recentScoreTimer < 0)
            {
                recentScore = 0;
                recentScoreTimer = 0;
            }
            recentScore -= recentScore * (2f - recentScoreTimer / recentScoreTimerDefault) * Time.deltaTime;
            if (recentScore <= 0 || (score - recentScore) < 0)
            {
                recentScore = 0;
                recentScoreTimer = 0;
            }
        }
    }

    Dot CreateDot(Vector2Int position)
    {
        GameObject newDot = dotsPool.GetPooledObject();
        newDot.transform.SetParent(dotsParentTransform, true);
        Dot newDotSctipt = newDot.GetComponent<Dot>();
        newDotSctipt.Activate(availableColors, position);
        dots[position.x, position.y] = newDotSctipt;
        return newDotSctipt;
    }

    void FinishConnection(List<Dot> dots)
    {
        StartCoroutine(FinishConnectionCoroutine(dots));
    }

    IEnumerator FinishConnectionCoroutine(List<Dot> dots)
    {
        SetInputAvailable(false);

        int bonusScore = 0;
        for (int i = 0; i < dots.Count; i++)
        {
            bonusScore += i * 10;
            Vector2Int position = dots[i].Position;
            this.dots[position.x, position.y] = null;
            dots[i].Deactivate();
        }

        AddScore(bonusScore);

        yield return new WaitForSeconds(1.5f);

        GroupDots();

        yield return new WaitForSeconds(1);

        FillDots();

        SetInputAvailable(true);
    }

    void AddScore(int score)
    {
        this.score += score;
        this.recentScore += score;
        recentScoreTimer = recentScoreTimerDefault;
    }

    void GroupDots()
    {
        for (int i = 0; i < dots.GetLength(0); i++)
        {
            List<Dot> activeDots = new List<Dot>();

            for (int j = 0; j < dots.GetLength(1); j++)
            {
                if (dots[i, j] != null) activeDots.Add(dots[i, j]);
            }

            for (int j = 0; j < activeDots.Count; j++)
            {
                Vector2Int dotPosition = activeDots[j].Position;
                dots[dotPosition.x, dotPosition.y] = null;
                dots[i, j] = activeDots[j];

                activeDots[j].MoveToPosition(new Vector2Int(i, j));
            }
        }
    }

    void FillDots()
    {
        for (int i = 0; i < dots.GetLength(0); i++)
        {
            for (int j = 0; j < dots.GetLength(1); j++)
            {
                if (dots[i,j] == null) CreateDot(new Vector2Int(i, j));
            }
        }
    }

    void SetInputAvailable(bool inputAvailable)
    {
        this.inputAvailble = inputAvailable;
        this.dotConnector.SetInputAvailable(inputAvailable);
    }

}
