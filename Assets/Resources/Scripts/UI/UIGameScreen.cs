﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIGameScreen : MonoBehaviour
{
    [SerializeField] Level level;
    [SerializeField] GameObject startGameObject;
    [SerializeField] UIScoreText textScore;

   
    
    // Start is called before the first frame update
    void Start()
    {
        
    }


    // Update is called once per frame
    void Update()
    {
        if (level == null) return;

        textScore.SetScore(level.Score, level.RecentScore);
    }

    public void StartGame()
    {
        level.StartGame();
        startGameObject.gameObject.SetActive(false);
    }

    public void RestartGame()
    {
        level.Restart();
    }
}
