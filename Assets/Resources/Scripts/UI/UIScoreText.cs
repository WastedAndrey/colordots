﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class UIScoreText : MonoBehaviour
{
    [SerializeField] TextMeshProUGUI textScore;
    [SerializeField] TextMeshProUGUI textRecentScore;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void SetScore(float score, float recentScore)
    {
        textScore.text = ((int)(score - recentScore)).ToString();
        if (recentScore >= 1)
        {
            textRecentScore.gameObject.SetActive(true);
            textRecentScore.text = "+" + ((int)recentScore).ToString();
        } 
        else textRecentScore.gameObject.SetActive(false);
    }
}
