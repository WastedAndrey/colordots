﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class SaveData
{
    public int score;
    public List<Color> colors;

    public SaveData(int score, List<Color> colors)
    {
        this.score = score;
        this.colors = colors;
    }

}
