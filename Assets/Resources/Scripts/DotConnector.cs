﻿using System.Collections.Generic;
using UnityEngine;

public delegate void ConnectionFinishedEvent(List<Dot> connectedDots);

public class DotConnector : MonoBehaviour
{
    [SerializeField] ObjectPool lightningPool;

    bool inputAvailable = false;
    bool connecting = false;
    List<Dot> connectedDots = new List<Dot>();
    List<LightningEffect> lightningEffects = new List<LightningEffect>();
    Color currentColor;
    Vector2Int currentPosition;

    public ConnectionFinishedEvent connectionFinished;

    // Start is called before the first frame update
    void Start()
    {
        lightningPool.Init(PrefabManager.Instance.prefabLightning, 10);
    }

    public void SetInputAvailable(bool inputAvailable)
    {
        this.inputAvailable = inputAvailable;
    }

    // Update is called once per frame
    void Update()
    {
        if (inputAvailable == false)
        {
            return;
        }

        if (Input.GetMouseButtonUp(0))
        {
            FinishConnection();
        }

        if (Input.GetMouseButton(0))
        {
            Dot dot = TryRaycastDot();

            if (dot != null)
            {
                if (connecting == false)
                {
                    StartConnection(dot);
                }
                else
                {
                    TryAddConnection(dot);
                }
            }
        }
    }

   

    Dot TryRaycastDot()
    {
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit;

        if (Physics.Raycast(ray, out hit))
        {
            if (hit.collider.gameObject.tag == "Dot")
            {
                GameObject dot = hit.collider.gameObject;
                Dot dotScript = dot.GetComponent<Dot>();
                return dotScript;
            }
        }

        return null;
    }

    void StartConnection(Dot dot)
    {
        connecting = true;
        currentColor = dot.Color;
        currentPosition = dot.Position;
        connectedDots.Add(dot);
        dot.Connect();

    }

    void TryAddConnection(Dot dot)
    {
        if (dot.Color != currentColor) return;
        if (Mathf.Abs(dot.Position.x - currentPosition.x) + Mathf.Abs(dot.Position.y - currentPosition.y) != 1) return;

        for (int i = 0; i < connectedDots.Count; i++)
        {
            if (dot == connectedDots[i]) return;
        }

        connectedDots.Add(dot);
        currentPosition = dot.Position;
        dot.Connect();
        CreateLightning(connectedDots[connectedDots.Count - 2].transform.position, dot.transform.position);
    }

    void FinishConnection()
    {
        connecting = false;
        if (connectedDots.Count > 1) connectionFinished?.Invoke(connectedDots);

        for (int i = 0; i < connectedDots.Count; i++)
        {
            connectedDots[i].Disconnect();
        }

        connectedDots.Clear();

        for (int i = 0; i < lightningEffects.Count; i++)
        {
            lightningEffects[i].Deactivate();
        }
        lightningEffects.Clear();
    }

    void CreateLightning(Vector3 start, Vector3 finish)
    {
        GameObject newLightning = lightningPool.GetPooledObject();
        LightningEffect newLightningScript = newLightning.GetComponent<LightningEffect>();
        newLightningScript.Activate();
        newLightningScript.SetPositions(start, finish);
        lightningEffects.Add(newLightningScript);
    }
}
