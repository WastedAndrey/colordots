﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectPool : MonoBehaviour
{
    GameObject prefab;
    public List<GameObject> pooledObjects;

    public void Init(GameObject prefab, int amountToPool)
    {
        if (prefab == null) 
        {
            Debug.Log("Object pool initialization failed. Prefab is null.");
            return;
        }

        this.prefab = prefab;
        pooledObjects = new List<GameObject>();

        for (int i = 0; i < amountToPool; i++)
        {
            CreateNewObject();
        }
    }

    GameObject CreateNewObject()
    {
        if (prefab == null)
        {
            Debug.Log("Object pool is not inited.");
            return null;
        }

        GameObject newObject = Instantiate(prefab);
        newObject.SetActive(false);
        newObject.transform.SetParent(this.transform, true);
        pooledObjects.Add(newObject);
        return newObject;
    }

    public GameObject GetPooledObject()
    {
        if (prefab == null)
        {
            Debug.Log("Object pool is not inited.");
            return null;
        }


        for (int i = 0; i < pooledObjects.Count; i++)
        {
            if (pooledObjects[i].activeInHierarchy == false)
            {
                return pooledObjects[i];
            }
        }

        GameObject newObject = CreateNewObject();
        return newObject;
    }
}
