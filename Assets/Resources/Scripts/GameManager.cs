﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.SceneManagement;


/// <summary>
/// Класс игрового менеджера. Правда тут ему особо нечем управлять. А так он мог бы управлять сценами и всякиим таким
/// </summary>
public class GameManager : MonoBehaviour
{
    [SerializeField] Level level;
    string path;

    private static GameManager _instance;
    public static GameManager Instance
    {
        get
        {
            if (_instance == null)
            {
                _instance = GameObject.FindObjectOfType<GameManager>();
            }

            return _instance;
        }
    }


    void Awake()
    {
        if (_instance != null) Destroy(this.gameObject);
        if (_instance == null) _instance = this;

       DontDestroyOnLoad(gameObject);
    }

    private void Start()
    {
        path = Application.persistentDataPath + @"/save.txt";

        SaveData saveData = LoadFromTextFile(path);
        if (saveData != null) level.Init(saveData);
        else level.Init();
    }

 

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            Application.Quit();
        }
    }

    private void OnApplicationQuit()
    {
        SaveData saveData = level.GetSaveData();
        SaveToTextFile(path, saveData);
    }

    #region Save/Load
    public void SaveToTextFile(string path, SaveData saveData)
    {
        TryCreateFile(path);

        string json = JsonUtility.ToJson(saveData);
        File.WriteAllText(path, json);
    }

    public SaveData LoadFromTextFile(string path)
    {
        if (!File.Exists(path)) return null;

        string json = File.ReadAllText(path);
        SaveData saveData = JsonUtility.FromJson<SaveData>(json);

        return saveData;
    }

    public void SaveToBinaryFile(string path, SaveData saveSata)
    {
        TryCreateFile(path);

        string json = JsonUtility.ToJson(saveSata);

        using (BinaryWriter writer = new BinaryWriter(File.Open(path, FileMode.Create)))
        {
            writer.Write(json);
        }
    }

    public SaveData LoadFromBinaryFile(string path)
    {
        if (!File.Exists(path)) return null;

        string json = "";
        using (BinaryReader reader = new BinaryReader(File.Open(path, FileMode.Open)))
        {
            json = reader.ReadString();
        }

        SaveData saveData = JsonUtility.FromJson<SaveData>(json);

        return saveData;
    }



    /// <summary>
    /// метод для проверки существования директории и файла. Если директории или файла нет, то создает.
    /// </summary>
    /// <param name="path"></param>
    /// <returns></returns>
    bool TryCreateFile(string path)
    {
        try
        {
            string dirPath = Path.GetDirectoryName(path);

            if (!Directory.Exists(dirPath))
            {
                Directory.CreateDirectory(dirPath);
            }

            if (!File.Exists(path))
            {
                FileStream stream = File.Create(path);
                stream.Close();
                return true;
            }

            return false;

        }
        catch (System.Exception e)
        {
            Debug.Log(e);
            return false;
        }
    }
    #endregion

   
}
